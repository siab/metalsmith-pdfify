import Promise from 'bluebird';
import pdc from 'pdc';

export const pandoc = (args, from, to, src) =>
  new Promise((resolve, reject) => {
    pdc(src, from, to, args, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });

export default {
  pandoc,
};
