import winston from 'winston';
import {plugin as pdfify} from './pdfify';

// Setup logging
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {timestamp: true, colorize: true});

module.exports = pdfify;
