import _ from 'lodash/fp';
import Promise from 'bluebird';
import winston from 'winston';
import fs from 'fs';
import path from 'path';

import {pandoc} from './pandoc';

Promise.promisifyAll(fs);

const STRIP_PATH = /\/media/g;

export const plugin = (opts = {}) => {
  const sources = _.get('sources', opts) || [];
  const pandocArgs = _.get('pandoc', opts) || [];
  const destDir = _.get('destination', opts) || 'pdfs';

  return (files, metalsmith, done) => {
    Promise.mapSeries(sources, s => {
      const data = files[s].contents.toString().replace(STRIP_PATH, 'media');
      const src = `${metalsmith._source}/${s}.pandoc`;
      const newFilename = path.dirname(s).split('/').join('_');
      const dest = `${metalsmith._destination}/${destDir}/${newFilename}.pdf`;
      const args = _.concat(pandocArgs, [`-o${dest}`, src]);

      winston.info(`Generating PDF for ${s} in ${dest}.`);

      return fs.mkdirAsync(`${metalsmith._destination}/${destDir}`)
               .catch(() => winston.debug(`${destDir} already exists. Skipping.`))
               .then(() => fs.writeFileAsync(src, data))
               .then(() => pandoc(args, 'markdown', 'latex', src))
               .then(() => fs.unlinkAsync(src));
    })
    .then(done)
    .catch(done);
  };
};

export default { plugin };
