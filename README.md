# PDF Generator

## Requirements

In order to generate PDF's, pandoc and LaTex must be installed. On Debian run
the following commands to install all necessary requirements:

```
apt-get install haskell-platform texlive pandoc pandoc-citeproc
```

See the [Pandoc Installation](http://pandoc.org/installing.html) page for
details for other platforms.

## Installation

```
git clone git@git.tacticaltech.org:ttc/pdf-generator.git
cd pdf-generator
npm install
```
